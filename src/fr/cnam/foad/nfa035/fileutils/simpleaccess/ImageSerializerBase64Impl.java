package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;


/**
 * Permet l'encodage et deséencodage de l'image en base64.
 * 
 * L'image passé en paramètre doit d'abord être convertie en byte[] car la méthode d'encodage
 * n'accepte que ce type de variable.
 * Un objet de type String étant attendu en sortie, il faut utiliser la méthode encodeToString().
 * 
 * Le décodage renvoi un byte[], qui est directement traité après son renvoi.
 * 
 */


public class ImageSerializerBase64Impl extends ImageSerializer {
	
	public String serialize(File image) throws IOException {
				
		BufferedImage bi = ImageIO.read(image);
		ByteArrayOutputStream baos = new ByteArrayOutputStream ();
		ImageIO.write(bi,"png",baos);
		byte[] bytes=baos.toByteArray();
		String tabOctet = Base64.getEncoder().encodeToString(bytes);
							
		return tabOctet;
	}
	
	public byte[] deserialize(String encodedImage) {
		
		byte [] decodageImage=Base64.getDecoder().decode(encodedImage);
		return decodageImage;
	}

}
